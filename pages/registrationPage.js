class RegistrationPage {
  get emailInput(){
    return $('[name="email"]');
  }
  
  get firstNameInput(){
    return $('[name="firstName"]');
  }
  
  get lastNameInput(){
    return $('[name="lastName"]');
  }

  get passwordInput(){
    return $('[name="password"]');
  }

  get passwordConfirmationInput(){
    return $('[name="passwordConfirmation"]');
  }

  get registerButton(){
    return $('<button />');
  }

  get errorPanel(){
    return $("span:nth-child(1)");
  }
}
  
module.exports = new RegistrationPage();