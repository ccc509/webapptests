class SettingPage{

  get deleteAccountButton(){
    return $('//*[@id="root"]/div[3]/button');
  }

  get editEmailButton(){
    return $('//*[@id="root"]/div[3]/table/tbody/tr[1]/td[3]/button');
  }

  get editPasswordButton(){
    return $('//*[@id="root"]/div[3]/table/tbody/tr[2]/td[3]/button');
  }

  get newEmailInput(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[1]/input');
  }

  get passwordInput(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[2]/input');
  }

  get submitNewEmailButton(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[3]/button[1]');
  }

  get oldPasswordInput(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[1]/input');
  }

  get newPasswordInput(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[2]/input');
  }

  get newPasswordConfirmation(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[3]/input');
  }

  get submitNewPasswordButton(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[4]/button[1]');
  }

  get errorPanelOfUpdatingEmail(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[3]/span');
  }

  get errorPanelOfUpdatingPassword(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[4]/span');
  }

  get deleteAccountEmailInput(){
    return $('//*[@id="react-confirm-alert"]/div/div/div/form/div[1]/input');
  }

  get deleteAccountConfirmButton(){
    return $('//*[@id="react-confirm-alert"]/div/div/div/form/div[2]/button[1]');
  }
}

module.exports = new SettingPage();