class LogInPage {
  get url(){
    return "http://localhost:3000/login";
  }

  get emailInput(){
    return $('[name="email"]');
  }

  get passwordInput(){
    return $('[name="password"]');
  }

  get logInButton(){
    return $('<button />');
  }

  get errorPanel(){
    return $("span:nth-child(1)");
  }
}

module.exports = new LogInPage();