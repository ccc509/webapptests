const {
  getTextValue
} = require('./helpers');

class DashBoard{
  get viewMapButton(){   
    return $('.view-map-button=VIEW MAP');
  }

  get cellContainsTopMapName(){
    return $('//*[@id="root"]/div[3]/div[2]/div[2]/table/tbody/tr[1]/td[1]');
  }

  get webTitleOfTopMap(){
    const mapName = getTextValue(this.cellContainsTopMapName);
    return `Mimica : ${mapName}`;
  }
}
  
module.exports = new DashBoard();