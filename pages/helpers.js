function setValue(input, value){
  input.waitForExist({ timeout: 5000 });
  input.setValue(value);
}

function clickButton(button){
  button.waitForExist({ timeout: 5000 });
  button.click();
}

function getTextValue(element){
  element.waitForExist({ timeout: 5000 });
  return element.getText()
}

module.exports = {setValue, clickButton, getTextValue};