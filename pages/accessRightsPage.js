class AccessRightsPage {
  get url(){
    return "http://localhost:3000/access-rights";
  }

  get addButton(){
    return $('//*[@id="root"]/div[3]/div[2]/div[1]/button[1]');
  }

  get emailInput(){
    return $('[name="email"]');
  }

  get userTypeSelector(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[2]/div/div');
  }

  get adminUserType(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[2]/div/ul/li[2]');
  }

  get createNewUserButton(){
    return $('//*[@id="react-confirm-alert"]/div/div/form/div[3]/button[1]');
  }
}

module.exports = new AccessRightsPage();