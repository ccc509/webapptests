const {
  ADMIN_EMAIL_ADDRESS, 
  ADMIN_PASSWORD
} = require("./constants");

const dashBoard = require('../pages/dashBoard');
const {setValue, clickButton, getTextValue} = require('../pages/helpers');

describe('Canvas', () => {
  it('Should be able to navigate up and down', () => {
    browser.url('http://localhost:3000/');
    // Log in as an admin
    $('[name="email"]').setValue(ADMIN_EMAIL_ADDRESS);
    $('[name="password"]').setValue(ADMIN_PASSWORD);
    $('<button />').click();
    
    browser.pause(1000);
  
    clickButton(dashBoard.viewMapButton);

    
  
    browser.pause(1000);
  
    browser.switchWindow(dashBoard.webTitleOfTopMap);
    browser.pause(1000);
    $('//*[@id="root"]/div[3]/div[1]/div[2]/div/div[3]/span[2]').click();
    browser.pause(1000);
    browser.keys("E");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowUp");
    browser.keys("ArrowUp");
    browser.keys("ArrowUp");
    browser.pause(1000);
    // Click BPMN button
    //$('//*[@id="root"]/div[3]/div[1]/div[1]/div[1]/div[2]/div/ul/li[1]/a').click();
  
    // browser.pause(1000);
  
    
    browser.pause(1000);
  })
  })