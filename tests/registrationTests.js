const assert = require('assert')
const registrationPage = require('../pages/registrationPage');
const {
  setValue, 
  clickButton, 
  getTextValue
} = require('../pages/helpers');

describe('Register new user', () => {
  before(() => {   
    browser.url('http://localhost:3000/register');
  })

  beforeEach(() => {
    browser.refresh();
  })

  it('Should return error when password is invalid', () => {
    setValue(registrationPage.firstNameInput, "Test");
    setValue(registrationPage.lastNameInput, "Test");
    setValue(registrationPage.emailInput, "test@mimica.ai");
    setValue(registrationPage.passwordInput, "Test");
    setValue(registrationPage.passwordConfirmationInput, "Test");
    clickButton(registrationPage.registerButton);

    const errorMessage = getTextValue(registrationPage.errorPanel);
    assert.strictEqual(errorMessage, 
    "Password should be minimum 8 characters including at least one letter, one number, and one symbol.");
  })

  it('Should return error when email address is not white listed', () => {
    setValue(registrationPage.firstNameInput, "Test");
    setValue(registrationPage.lastNameInput, "Test");
    setValue(registrationPage.emailInput, "ccc@gmail.com");
    setValue(registrationPage.passwordInput, "Test_1234");
    setValue(registrationPage.passwordConfirmationInput, "Test_1234");
    clickButton(registrationPage.registerButton);

    const errorMessage = getTextValue(registrationPage.errorPanel);
    assert.strictEqual(errorMessage, "Email is not whitelisted.");
  })

  it('Should return error when email address is invalid', () => {
    setValue(registrationPage.firstNameInput, "Test");
    setValue(registrationPage.lastNameInput, "Test");
    setValue(registrationPage.emailInput, "Test");
    setValue(registrationPage.passwordInput, "Test_1234");
    setValue(registrationPage.passwordConfirmationInput, "Test_1234");
    clickButton(registrationPage.registerButton);

    const errorMessage = getTextValue(registrationPage.errorPanel);
    assert.strictEqual(errorMessage, "Please input valid email address.");
  })

  it('Should return error when password mismatches with username', () => {
    setValue(registrationPage.firstNameInput, "Test");
    setValue(registrationPage.lastNameInput, "Test");
    setValue(registrationPage.emailInput, "test2@mimica.ai");
    setValue(registrationPage.passwordInput, "Test_1234");
    setValue(registrationPage.passwordConfirmationInput, "Test_1234");
    clickButton(registrationPage.registerButton);

    const errorMessage = getTextValue(registrationPage.errorPanel);
    assert.strictEqual(errorMessage, "Email is not whitelisted.");
  })
})