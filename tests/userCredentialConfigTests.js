const assert = require('assert');

const {
  ADMIN_EMAIL_ADDRESS, 
  ADMIN_PASSWORD
} = require("./constants");
const {
  setValue, 
  clickButton, 
  getTextValue
} = require('../pages/helpers');
const logInPage = require('../pages/logInPage');
const settingPage = require('../pages/settingPage');
const actionMenu = require('../elements/actionMenu');

const newEmail = "newEmail@mimicam.ai";
const newPassword = "NewPassword-1234!";

beforeEach(() => {
  browser.refresh();
})

describe('Modify user credentials', () => {
  it('Should not modify email address if the password is incorrect', () => {
    browser.url(logInPage.url);
    // Log in as an admin
    setValue(logInPage.emailInput, ADMIN_EMAIL_ADDRESS);
    setValue(logInPage.passwordInput, ADMIN_PASSWORD);
    clickButton(logInPage.logInButton);
    
    // Click setting button
    clickButton(actionMenu.actionMenu);
    clickButton(actionMenu.settings);

    // Click email edit button
    clickButton(settingPage.editEmailButton);

    // Set new email address
    setValue(settingPage.newEmailInput, newEmail);

    // set incorrect password
    setValue(settingPage.passwordInput, "Incorrect_password");

    //Click submit button
    clickButton(settingPage.submitNewEmailButton);

    const errorMessage = getTextValue(settingPage.errorPanelOfUpdatingEmail);
    assert.strictEqual(errorMessage, "Password does not match.");
  })

  it('Should not modify email address if the password is missing', () => {
    // Click email edit button
    clickButton(settingPage.editEmailButton);
  
    // Set new email address
    setValue(settingPage.newEmailInput, newEmail);
  
    //Click submit button
    clickButton(settingPage.submitNewEmailButton);
  
    const errorMessage = getTextValue(settingPage.errorPanelOfUpdatingEmail);
    assert.strictEqual(errorMessage, "Please input password.");
  })

  it('Should not modify email address if the email address is invalid', () => {
    // Click email edit button
    clickButton(settingPage.editEmailButton);
  
    // Set new email address
    setValue(settingPage.newEmailInput,  "InvalidEmail");
  
    //Click submit button
    clickButton(settingPage.submitNewEmailButton);
  
    const errorMessage = getTextValue(settingPage.errorPanelOfUpdatingEmail);
    assert.strictEqual(errorMessage, "Please input valid email address.");
  })

  it('Should modify email address', () => {
    // Click email edit button
    clickButton(settingPage.editEmailButton);
  
    // Set new email address
    setValue(settingPage.newEmailInput,  newEmail);

    // Set password
    setValue(settingPage.passwordInput, ADMIN_PASSWORD);
  
    //Click submit button
    clickButton(settingPage.submitNewEmailButton);

    // Should work
  })

  it('Should not update password if old password is missing', () => {
    // Click password edit button
    clickButton(settingPage.editPasswordButton);
  
    // Old password
    setValue(settingPage.oldPasswordInput, "");

    // New password
    setValue(settingPage.newPasswordInput, "");

    // Confirm new password
    setValue(settingPage.newPasswordConfirmation, "");

    // Click submit button
    clickButton(settingPage.submitNewPasswordButton);

    // Error message
    const errorMessage = getTextValue(settingPage.errorPanelOfUpdatingPassword);
    assert.strictEqual(errorMessage, "Please input password.");
  })

  it('Should not update password if old password is incorrect', () => {
    // Click password edit button
    clickButton(settingPage.editPasswordButton);

    // Old password
    setValue(settingPage.oldPasswordInput, "IncorrectPassword");

    // New password
    setValue(settingPage.newPasswordInput, newPassword);

    // Confirm new password
    setValue(settingPage.newPasswordConfirmation, newPassword);

    // Click submit button
    clickButton(settingPage.submitNewPasswordButton);
    
    // Error message
    const errorMessage = getTextValue(settingPage.errorPanelOfUpdatingPassword);
    assert.strictEqual(errorMessage, "Password does not match.");
  })

  it('Should not update password if new password is missing', () => {
    // Click password edit button
    clickButton(settingPage.editPasswordButton);

    // Old password
    setValue(settingPage.oldPasswordInput, ADMIN_PASSWORD);

    // New password
    setValue(settingPage.newPasswordInput, "");

    // Confirm new password
    setValue(settingPage.newPasswordConfirmation, "");

    // Click submit button
    clickButton(settingPage.submitNewPasswordButton);
    
    // Error message
    const errorMessage = getTextValue(settingPage.errorPanelOfUpdatingPassword);
    assert.strictEqual(errorMessage, "Please input new password.");
  })

  it('Should not update password if confirmed new password is missing', () => {
    // Click password edit button
    clickButton(settingPage.editPasswordButton);
  
    // Old password
    setValue(settingPage.oldPasswordInput, ADMIN_PASSWORD);

    // New password
    setValue(settingPage.newPasswordInput, newPassword);

    // Confirm new password
    setValue(settingPage.newPasswordConfirmation, "");

    // Click submit button
    clickButton(settingPage.submitNewPasswordButton);
    
    // Error message
    const errorMessage = getTextValue(settingPage.errorPanelOfUpdatingPassword);
    assert.strictEqual(errorMessage, "Please input confirm password.");
  })

  it('Should not update password if two new password do not match', () => {
    // Click password edit button
    clickButton(settingPage.editPasswordButton);
  
    // Old password
    setValue(settingPage.oldPasswordInput, ADMIN_PASSWORD);

    // New password
    setValue(settingPage.newPasswordInput, newPassword);

    // Confirm new password
    setValue(settingPage.newPasswordConfirmation, "Incorrect");

    // Click submit button
    clickButton(settingPage.submitNewPasswordButton);
    
    // Error message
    const errorMessage = getTextValue(settingPage.errorPanelOfUpdatingPassword);
    assert.strictEqual(errorMessage, "Password confirmation mismatch.");
  })

  it('Should not update password if password is invalid', () => {
    // Click password edit button
    clickButton(settingPage.editPasswordButton);
  
    // Old password
    setValue(settingPage.oldPasswordInput, ADMIN_PASSWORD);

    // New password
    setValue(settingPage.newPasswordInput, "xxx");

    // Confirm new password
    setValue(settingPage.newPasswordConfirmation, "xxx");

    // Click submit button
    clickButton(settingPage.submitNewPasswordButton);
    
    // Error message
    const errorMessage = getTextValue(settingPage.errorPanelOfUpdatingPassword);
    assert.strictEqual(errorMessage, 
    "Password should be minimum 8 characters including at least one letter, one number, and one symbol.");
  })

  it('Should update password', () => {
    // Click password edit button
    clickButton(settingPage.editPasswordButton);
  
    // Old password
    setValue(settingPage.oldPasswordInput, ADMIN_PASSWORD);

    // New password
    setValue(settingPage.newPasswordInput, newPassword);

    // Confirm new password
    setValue(settingPage.newPasswordConfirmation, newPassword);

    // Click submit button
    clickButton(settingPage.submitNewPasswordButton);
    
    // Should work
  })

  it('Should be able to log in using new email and password', () => {
    // Log out
    clickButton(actionMenu.actionMenu);
    clickButton(actionMenu.logout);

    // Log in using new credential
    setValue(logInPage.emailInput, newEmail);
    setValue(logInPage.passwordInput, newPassword);
    clickButton(logInPage.logInButton);
    //TODO: make sure it is loaded
    browser.pause(2000);
    // Should work
  })

  it('Should change the old credential back', () => {
    
    // Click setting button
    clickButton(actionMenu.actionMenu);
    clickButton(actionMenu.settings);

    // Click email edit button
    clickButton(settingPage.editEmailButton);
  
    // Set new email address
    setValue(settingPage.oldPasswordInput, ADMIN_EMAIL_ADDRESS);

    // Set password
    setValue(settingPage.newPasswordInput, newPassword);
  
    //Click submit button
    clickButton(settingPage.submitNewEmailButton);

    // Click password edit button
    clickButton(settingPage.editPasswordButton);
  
    // Old password
    setValue(settingPage.oldPasswordInput, newPassword);

    // New password
    setValue(settingPage.newPasswordInput, ADMIN_PASSWORD);

    // Confirm new password
    setValue(settingPage.newPasswordConfirmation, ADMIN_PASSWORD);

    // Click submit button
    clickButton(settingPage.submitNewPasswordButton);
    
    // Should work
  })

  it('Should be able to log in using old email and password', () => {
    // Log out
    clickButton(actionMenu.actionMenu);
    clickButton(actionMenu.logout);

    // Log in using new credential
    setValue(logInPage.emailInput, ADMIN_EMAIL_ADDRESS);
    setValue(logInPage.passwordInput, ADMIN_PASSWORD);
    clickButton(logInPage.logInButton);
    
    // Should work
  })
})