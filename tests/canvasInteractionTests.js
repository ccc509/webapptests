const {
  ADMIN_EMAIL_ADDRESS, 
  ADMIN_PASSWORD
} = require("./constants");
const {
  setValue, 
  clickButton, 
  getTextValue
} = require('../pages/helpers');
const logInPage = require('../pages/logInPage');
const dashBoard = require('../pages/dashBoard');
const canvas = require('../elements/canvas');
const rightPanel = require('../elements/rightPanel');

beforeEach(() => {
  browser.refresh();
  browser.pause(1000);
  clickButton(canvas.depthOneButton);
  browser.pause(1000);
})

before(() => {
  browser.url(logInPage.url);
  setValue(logInPage.emailInput, ADMIN_EMAIL_ADDRESS);
  setValue(logInPage.passwordInput, ADMIN_PASSWORD);
  clickButton(logInPage.logInButton);
  browser.pause(1000);
  clickButton(dashBoard.viewMapButton);
  browser.pause(1000);
  browser.switchWindow(dashBoard.webTitleOfTopMap);
})

describe('Canvas', () => {
  it('Should be able to navigate up and down', () => {
    browser.pause(1000);
    browser.keys("E");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowUp");
    browser.keys("ArrowUp");
    browser.keys("ArrowUp");
    browser.pause(1000);   
    browser.pause(1000);
  })

  it('Should be able to move node up and down', () => {
    browser.keys("E");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("R");
    browser.keys("R");
    browser.keys("R");
    browser.keys("R");
    browser.keys("R");
    browser.keys("R");
    browser.keys("R");
    browser.keys("R");
    browser.keys("R");
    browser.keys("R");
    browser.keys("R");
    browser.keys("R");
    browser.keys("R");   
  })

  it('Should be able to move node up and down', () => {
    browser.keys("E");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("F");
    browser.keys("F");
    browser.keys("F");
    browser.keys("F");
    browser.keys("F");
    browser.keys("F");
    browser.keys("F");
    browser.keys("F");
    browser.keys("F");
    browser.keys("F");
    browser.keys("F");
    browser.keys("F");
    browser.keys("F");
  })

  it('Should be able to add and reduce space', () => {
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("G");
    browser.keys("G");
    browser.keys("G");
    browser.keys("G");
    browser.keys("G");
    browser.keys("G");
    browser.keys("G");
    browser.keys("T");
    browser.keys("T");
    browser.keys("T");
    browser.keys("T");
    browser.keys("T");
    browser.keys("T");
  })

  it('Should be able to create decision point', () => {
    browser.pause(1000);
    browser.keys("E");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("D");
    browser.keys("ArrowDown");
    browser.pause(2000);
    setValue(rightPanel.headerLabelEditor, "Decision value\n");
  })

  it('Should be able set node label', () => {
    browser.keys("E");
    browser.keys("ArrowDown");
    browser.pause(2000);
    setValue(rightPanel.headerLabelEditor, "New label value\n");
    //TODO: label has been set
  })

  it('Should be able set node note', () => {
    browser.keys("E");
    browser.keys("ArrowDown");
    browser.pause(2000);
    setValue(rightPanel.noteEditor, "New note value\n");
    //TODO: note has been set
  })

  it('Should be able to create interrupt node', () => {
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("I");
    browser.keys("I");
    browser.keys("I");
    browser.keys("I");
    browser.keys("I");
    browser.keys("I");
    browser.keys("I");
    browser.keys("I");
    browser.keys("I");
    browser.keys("I");
    browser.keys("I");
  })

  it('Should be able to COPY node', () => {
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("C");
    browser.keys("ArrowRight");
    browser.keys("V");
    browser.keys("V");
    browser.keys("V");
    browser.keys("V");
    browser.keys("V");
    browser.keys("V");
    browser.keys("V");
    browser.keys("V");
    browser.keys("V");
    browser.keys("V");
  })

  it('Should be able to duplicate depth', () => {
    browser.pause(1000);
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("N");
    browser.keys("N");
    browser.keys("N");
    browser.keys("N");
    browser.keys("N");
    browser.keys("N");
    browser.keys("N");
    browser.keys("N");
    browser.keys("N");
    browser.keys("N");
    browser.keys("N");
  })

  it('Should be able to open screenshot', () => {
    browser.pause(1000);
    browser.keys("E");
    browser.keys("ArrowDown");
    clickButton(rightPanel.screenshot);
    browser.pause(1000);
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowDown");
    browser.keys("ArrowUp");
    browser.keys("ArrowUp");
    browser.keys("ArrowUp");
    browser.keys("ArrowUp");
    //TODO: Make sure navigation worked, when exit, it is another task selected
  })
})