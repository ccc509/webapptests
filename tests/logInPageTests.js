const assert = require('assert')
const logInPage = require('../pages/logInPage')
const {setValue, clickButton, getTextValue} = require('../pages/helpers');

describe('Log in page', () => {
  before(() => {   
    browser.url('http://localhost:3000/');
  })

  beforeEach(() => {
    browser.refresh();
  })

  it('Should return error when email is empty', () => {
    setValue(logInPage.emailInput, "");
    setValue(logInPage.passwordInput, "");
    clickButton(logInPage.logInButton);

    const errorMessage = getTextValue(logInPage.errorPanel);
    assert.strictEqual(errorMessage, "Please input email address.");
  })
  
  it('Should return error when password is empty', () => {
    setValue(logInPage.emailInput, "test@test.com");
    setValue(logInPage.passwordInput, "");
    clickButton(logInPage.logInButton);

    const errorMessage = getTextValue(logInPage.errorPanel);
    assert.strictEqual(errorMessage, "Please input password.");
  })
  
  it('Should return error when email is empty', () => {
    setValue(logInPage.emailInput, "Whatever");
    setValue(logInPage.passwordInput, "");
    clickButton(logInPage.logInButton);

    const errorMessage = getTextValue(logInPage.errorPanel);
    assert.strictEqual(errorMessage, "Please input valid email address.");
  }) 
  
  it('Should return error when password and email address mismatch', () => {
    setValue(logInPage.emailInput, "test@test.com");
    setValue(logInPage.passwordInput, "wrong_password");
    clickButton(logInPage.logInButton);

    const errorMessage = getTextValue(logInPage.errorPanel);
    assert.strictEqual(errorMessage, "Email and password do not match.");
  })
})