const assert = require('assert')

const logInPage = require('../pages/logInPage')
const registrationPage = require('../pages/registrationPage');
const settingPage = require('../pages/settingPage');
const accessRightsPage = require('../pages/accessRightsPage');
const actionMenu = require('../elements/actionMenu');
const {setValue, clickButton} = require('../pages/helpers');
const {
  ADMIN_EMAIL_ADDRESS, 
  ADMIN_PASSWORD
} = require("./constants");

const newUserEmailAddress = "NewUser@mimica.ai";
const newUserPassword = "Test_1234";

describe('Access right page', () => {
  beforeEach(() => {
    browser.refresh();
  })

  it('Admin user should be able to create an user', () => {
    // Log in as an admin
    browser.url(logInPage.url);
    setValue(logInPage.emailInput, ADMIN_EMAIL_ADDRESS);
    setValue(logInPage.passwordInput, ADMIN_PASSWORD);
    clickButton(logInPage.logInButton);

    // Go to access rights page
    clickButton(actionMenu.actionMenu);
    clickButton(actionMenu.accessRights);
    
    // Create new user
    clickButton(accessRightsPage.addButton);
    setValue(accessRightsPage.emailInput, newUserEmailAddress);
    clickButton(accessRightsPage.userTypeSelector);
    clickButton(accessRightsPage.adminUserType);
    clickButton(accessRightsPage.createNewUserButton);

    // Log out
    clickButton(actionMenu.actionMenu);
    clickButton(actionMenu.logout);
  })

  it('Should be able to register an user with whitelisted email address', () => {
    // Go to register page and create user
    browser.url('http://localhost:3000/register');
    setValue(registrationPage.firstNameInput, "Test");
    setValue(registrationPage.lastNameInput, "Test");
    setValue(registrationPage.emailInput, newUserEmailAddress);
    setValue(registrationPage.passwordInput, newUserPassword);
    setValue(registrationPage.passwordConfirmationInput, newUserPassword);
    clickButton(registrationPage.registerButton);
  })

  it('An user should be able to delete his own account', () => {
    // Log in as the new user
    browser.url(logInPage.url);
    setValue(logInPage.emailInput, newUserEmailAddress);
    setValue(logInPage.passwordInput, newUserPassword);
    clickButton(logInPage.logInButton);

    // Click setting
    clickButton(actionMenu.actionMenu);
    clickButton(actionMenu.settings);
    
    // CLick delete account
    clickButton(settingPage.deleteAccountButton);
    setValue(settingPage.deleteAccountEmailInput, newUserPassword);
    clickButton(settingPage.deleteAccountConfirmButton);
  })
})