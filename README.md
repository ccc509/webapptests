# Web App Tests
 
## Introduction
This project is an User Acceptance Testing project of web app which automatically performs most user actions

## Setup

### Install Node Js

Download the latest version of Node Js from [here](https://nodejs.org/en/download/) and install it.

### Install Webdriver IO

Details of WebDriver IO can be found from [here](https://webdriver.io/)

Run the command below to install all packages:

```
npm install
```

In addition, make sure the version of your Chrome web browser is higher than 81 

Once both backend and front end service of the web app are running locally, run the command below to start testing:

```
npx wdio wdio.conf.js
```

Please also create a new Mimica user for testing purpose

```
username: testAdmin@mimica.ai
password: Password-1234!
```

## Testing Coverage

* Log in page
	* Should return error when email is empty
	* Should return error when password is empty
	* Should return error when email is empty
	* Should return error when password and email address mismatch

* Registration page
	* Should return error when password is invalid
	* Should return error when email address is not white listed
	* Should return error when email address is invalid
	* Should return error when password mismatches with username

* Access rights page
	* Admin user should be able to create an user
	* Should be able to register an user with whitelisted email address
	* An user should be able to delete his own account
* Setting page
    * Should not modify email address if the password is incorrect
    * Should not modify email address if the password is missing
    * Should not modify email address if the email address is invalid
    * Should modify email address
    * Should not update password if old password is missing
    * Should not update password if old password is incorrect
    * Should not update password if new password is missing
    * Should not update password if confirmed new password is missing
    * Should not update password if two new password do not match
    * Should not update password if password is invalid
    * Should update password
    * Should be able to log in using new email and password
    * Should change the old credential back
    * Should be able to log in using old email and password
* Canvas
	* Should be able to navigate up and down
	* Should be able to move node up and down
	* Should be able to move node up and down
	* Should be able to add and reduce space
	* Should be able to create decision point
	* Should be able set node label
	* Should be able set node note
	* Should be able to create interrupt node
	* Should be able to COPY node
	* Should be able to duplicate depth
	* Should be able to open screenshot