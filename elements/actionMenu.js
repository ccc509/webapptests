class ActionMenu {
  get actionMenu(){
    return $('//*[@id="root"]/div[1]/div[1]/div/span');
  }

  get logout(){
    return $('=Logout');
  }

  get accessRights(){
    return $('=Access Rights');
  }

  get settings(){
    return $('=Settings');
  }
}

module.exports = new ActionMenu();