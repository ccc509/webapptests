class RightPanel{
  get headerLabelEditor(){   
    return $('.panel-header-label');
  }

  get noteEditor(){
    return $('.panel-header-note');
  }

  get screenshot(){
    return $('.screenshot');
  }
}      
module.exports = new RightPanel();